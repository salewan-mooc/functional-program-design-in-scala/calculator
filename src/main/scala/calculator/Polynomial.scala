package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double], c: Signal[Double]): Signal[Double] = {
    Signal {
      val aa = a()
      val bb = b()
      val cc = c()
      bb * bb - 4 * aa * cc
    }
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double], c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      val discriminant = delta()
      if (discriminant < 0) Set()
      else {
        val aa = a()
        val bb = b()
        val cc = c()
        val root1 = (-bb + Math.sqrt(discriminant)) / (2 * aa)
        val root2 = (-bb - Math.sqrt(discriminant)) / (2 * aa)
        if (root1 == root2) Set(root1) else Set(root1, root2)
      }
    }
  }
}
