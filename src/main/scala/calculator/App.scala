import calculator.{Signal, Var}

/**
 * @author Sergey A. Lebedev (salewan@gmail.com) 26.04.2015.
 */
object App {

  def main(args: Array[String]) {
    val a = Var(5)
    val b = Signal(a() * 100)

    var l = List(1,2,3)
    println(l)
    l ::= 99
    println(l)
//    val b = Signal(a() + 10)
//    println(b())
  }
}
